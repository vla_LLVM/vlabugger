#include <math.h>

#include "llvm/IR/Instructions.h"
#include "vlabugger/ObfuscatingPass/ObfuscatingPass.h"
#include "vlabugger/Backend/Backend.h"


#undef  DEBUG_TYPE
#define DEBUG_TYPE "ObfuscatingPass"

using namespace vlabugger;

char ObfuscatingPass::ID = 0;
static RegisterPass<ObfuscatingPass> X("ObfuscatingPass", "Obfuscates the source code",false,false);

/*
 * Generates a random alphanumeric name with lenght len
 */
string ObfuscatingPass::genRandomName(const unsigned len)
{
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    string ret="";
    unsigned count=0;
    while(count<3 && ret==""){
        for (unsigned i = 0; i < len; ++i) {
            if(i){
                ret+= alphanum[rand() % (sizeof(alphanum) - 1)];
            }
            else{
                //do not allow numbers in the first position, not a valid ID
                ret+= alphanum[rand() % (sizeof(alphanum) - 11) + 10];
            }
            
        }
        if(this->isNameUsed(ret)){
            ret="";
        }
        else{
            this->markNameAsUsed(ret);
        }
        count++;
    }
    if(ret==""){
        llvm::dbgs()<<"cant generate random name\n";
        exit(2);
    }
    return ret;
}

/**
 * marks an identifier name as used
 * @param name
 * @return 
 */
void ObfuscatingPass::markNameAsUsed(string name)
{
    this->_used_names.insert(*(new std::pair<string,bool>(name,true)));
}

/*
 * returns true if the name is already used
 */
bool ObfuscatingPass::isNameUsed(string name)
{
    return this->_used_names.find(name)!=this->_used_names.end();
}

/**
 * renames F variables
 */
void ObfuscatingPass::renameVariables(Function *F)
{
    BasicBlock *bb=&F->getEntryBlock();
    for(BasicBlock::iterator I=bb->begin(),E=bb->end();I!=E;++I){
        AllocaInst *inst=llvm::dyn_cast<AllocaInst>(&(*I));
        if(inst){
            inst->setName(this->genRandomName(6));
        }
    }
}

/**
 * changes the names of the functions in M
 * @param M
 */
void ObfuscatingPass::renameVariables(Module *M)
{
    for(Module::iterator IF=M->begin(),IFE=M->end();IF!=IFE;++IF){
        Function *F=IF;
        if(F->isDeclaration()){
            continue;
        }
        this->renameVariables(F);
    }
}

bool ObfuscatingPass::runOnModule(Module &M) {
        ++DebuggerCounter;
        errs() << "Debugger: ";
        //errs().write_escaped(F.getName()) << '\n';

        llvm::dbgs()<<"Obfuscate pass is starting\n";

        this->renameVariables(&M);

        llvm::dbgs()<<"Obfuscate pass is finished\n";


        return false;
}

llvm::ModulePass * vlabugger::createObfuscatingPass()
{
    return new ObfuscatingPass();
}

