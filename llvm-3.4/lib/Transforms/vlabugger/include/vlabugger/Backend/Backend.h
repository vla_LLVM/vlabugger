#include <llvm/IR/Module.h>
#include <string>

using namespace std;
using namespace llvm;

namespace vlabugger {

class Backend{

private:
    Module * _M;
    string _output_file;
    
public:
	Backend(Module *M,string output_file);
	~Backend();
        
        void doCompilation();



};
}
