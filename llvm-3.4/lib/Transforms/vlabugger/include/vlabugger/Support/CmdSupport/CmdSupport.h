#if ! defined VLABUGGER_SUPPORT_CMDSUPPORT_H
#define VLABUGGER_SUPPORT_CMDSUPPORT_H

#include <string>
#include <signal.h>

#include <llvm/IR/Module.h>
#include <llvm/Support/Debug.h>

using std::string;

using llvm::Module;

/* to expand macros */
#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "="  VALUE(var)

#define VLABUGGER_VERSION "0.0.1"

#pragma message(VAR_NAME_VALUE(VLABUGGER_VERSION))

namespace vlabugger {


namespace CmdSupport {
    
    llvm::Module * getModuleFromCCode(string c_code);
    llvm::Module * getModuleFromFile(string InputFilename,string AddIncludes);
    string getSystemIncludesPath();
    string getFileName(string path);
    bool isExtensionFile(string file,string ext);
    std::string getexepath();
    void freeClangMemory(llvm::Module * M);
    void hide_linked_cmd_opts();
    
#ifndef LLVM_ON_WIN32
        
        void vb_signal_exit(int signal);
        void handler_segv(int signal);
        
        inline void capture_vb_signals()
        {
            #ifdef NDEBUG
                signal(SIGSEGV,&vlabugger::CmdSupport::handler_segv);
            #endif
            signal(SIGINT, &vlabugger::CmdSupport::vb_signal_exit);
        }

#else
        inline void capture_pw_signals()
        {
            return;
        }
#endif
    
}

}

#endif
