#include <map>
#include <string>

#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include <llvm/IR/Module.h>
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"


using namespace llvm;
using std::map;
using std::string;

STATISTIC(DebuggerCounter, "Counts number of functions greeted");

namespace {
  struct ObfuscatingPass : public ModulePass {
    static char ID; // Pass identification, replacement for typeid
    map<string,bool> _used_names;
    
    ObfuscatingPass() : ModulePass(ID) {}
    virtual bool runOnModule(Module &M);
    void markNameAsUsed(string name);
    bool isNameUsed(string name);
    string genRandomName(const unsigned len);
    void renameVariables(Module *M);
    void renameVariables(Function *F);

  };
 
}
namespace vlabugger{
    llvm::ModulePass * createObfuscatingPass();
}




