include_directories(
"${LLVM_MAIN_SRC_DIR}/lib/Transforms/vlabugger/include" 
)
add_subdirectory(Tools)
add_subdirectory(Support)
add_subdirectory(ObfuscatingPass)
add_subdirectory(Backend)