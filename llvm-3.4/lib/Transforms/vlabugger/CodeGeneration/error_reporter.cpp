# include <execinfo.h>         // For backtrace().
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <cstddef> // for std::size_t
#include <cxxabi.h>

/// Find the length of an array.
template<class T, std::size_t N>
inline size_t array_lengthof(T (&)[N]) {
  return N;
}

// PrintStackTrace - In the case of a program crash or fault, print out a stack
// trace so that the user has an indication of why and where we died.
//
// On glibc systems we have the 'backtrace' function, which works nicely, but
// doesn't demangle symbols.
void stack_print(FILE *FD) {
  static void* StackTrace[1024];
  // Use backtrace() to output a backtrace on Linux systems with glibc.
  int depth = backtrace(StackTrace,
                        static_cast<int>(array_lengthof(StackTrace)));
  int width = 0;
  for (int i = 0; i < depth; ++i) {
    Dl_info dlinfo;
    dladdr(StackTrace[i], &dlinfo);
    const char* name = strrchr(dlinfo.dli_fname, '/');

    int nwidth;
    if (name == NULL) nwidth = strlen(dlinfo.dli_fname);
    else              nwidth = strlen(name) - 1;

    if (nwidth > width) width = nwidth;
  }

  for (int i = 0; i < depth; ++i) {
    Dl_info dlinfo;
    dladdr(StackTrace[i], &dlinfo);
    fprintf(FD, "%-2d", i);

    const char* name = strrchr(dlinfo.dli_fname, '/');
    if (name == NULL) fprintf(FD, " %-*s", width, dlinfo.dli_fname);
    else              fprintf(FD, " %-*s", width, name+1);

    fprintf(FD, " %#0*lx",
            (int)(sizeof(void*) * 2) + 2, (unsigned long)StackTrace[i]);

    if (dlinfo.dli_sname != NULL) {
      fputc(' ', FD);
      int res;
      char *d=NULL;//char* d = abi::__cxa_demangle(dlinfo.dli_sname, NULL, NULL, &res);
      if (d == NULL) fputs(dlinfo.dli_sname, FD);
      else           fputs(d, FD);
      free(d);

      // FIXME: When we move to C++11, use %t length modifier. It's not in
      // C++03 and causes gcc to issue warnings. Losing the upper 32 bits of
      // the stack offset for a stack dump isn't likely to cause any problems.
      fprintf(FD, " + %u",(unsigned)((char*)StackTrace[i]-
                                     (char*)dlinfo.dli_saddr));
    }
    fputc('\n', FD);
  }
}

/**
 * here is vlabugging capturing a segmentation fault
 */
void vlabugger_sigsegv_handler(int sig){
    fprintf(stderr,"Ups Vlabugger has captured a segmentation fault\n");
    fprintf(stderr,"Here is the Stack Trace:\n");
    stack_print(stderr);
}

/**
 * captures signals for vlabugger operation
 */
void capture_vlabugger_signals()
{
    signal(SIGSEGV, vlabugger_sigsegv_handler);
}


//TESTING FUNCTIONS FROM NOW ON
// use following commands
// TO EMMIT LLVM IR ===>    clang -emit-llvm  error_reporter.cpp -S -o error_reporter.s
// TO EMMIT MODULE CPP CODE==> llc  -march=cpp -cppgen=program -o error_reporter_llvm.cpp <error_reporter.s 
// TO TEST IT ===>  g++ -rdynamic  error_reporter.cpp -o error_reporter.out -ldl
//
// UNCOMMENT THE FOLLOWING CODE TO TEST IT
//
//#include <unistd.h>
//
//int f_function2()
//{
//    printf("function 2\n");
//    stack_print(stdout);
//    return 3;
//}
//
//int f_function1(int a){
//    a+=1234;
//    printf("function 1\n");
//    return f_function2();
//}
//
//int main(int argc, char * argv[])
//{
//    capture_vlabugger_signals();
//    for(;;){
//        sleep(1);
//    }
//    return f_function1(5);
//}