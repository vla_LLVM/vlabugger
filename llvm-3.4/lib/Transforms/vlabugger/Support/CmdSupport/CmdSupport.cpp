#include "vlabugger/Support/CmdSupport/CmdSupport.h"

#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

//#ifdef NDEBUG
//        #undef VB_SYSTEM_CLANG
//#endif

#ifndef VB_SYSTEM_CLANG
    #include "clang/Frontend/TextDiagnosticPrinter.h"
    #include "clang/Basic/DiagnosticOptions.h"
    #include "clang/Basic/TargetInfo.h"
    #include "clang/Basic/TargetOptions.h"
    #include "clang/Frontend/CompilerInvocation.h"
    #include "clang/Frontend/Utils.h"
    #include "clang/Frontend/CompilerInstance.h"
    #include "clang/CodeGen/CodeGenAction.h"
    #include "clang/Lex/HeaderSearch.h"
    #include "clang/Lex/Preprocessor.h"
#endif


#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/CommandLine.h"

#include "vlabugger/vlabuggerExitErrorCodes.h"

using std::vector;

using namespace vlabugger;
using namespace llvm;

#ifndef VB_SYSTEM_CLANG
    using namespace clang;
#endif


#ifndef LLVM_ON_WIN32
        /**
         * capture signal ctrl+c
         */
        void CmdSupport::vb_signal_exit(int signal){
                // TODO DO TERMINAL RESET
                //	printf("%s\n",FORMAT_RESET);
                DEBUG(llvm::dbgs()<<"capturing signal "<<signal<<" for vlabugger exit\n";);
                exit(VB_ERR_SIGINT);
        }

        /**
         * capture signal segmentation fault
         */
        void CmdSupport::handler_segv(int signal){
                // TODO DO TERMINAL RESET
                //	printf("%s\n",FORMAT_RESET);
                std::cout<< "Something went wrong \n"<<std::endl;
                exit(VB_ERR_SIGSEGV);
        }
        
#endif


#ifdef __linux__
/**
 * get this binary executing path
 */
	std::string CmdSupport::getexepath()
	{
	  char result[ PATH_MAX ];
	  ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
	  return std::string( result, (count > 0) ? count : 0 );
	}
#else
        /**
 * get this binary executing path
 */
	std::string CmdSupport::getexepath()
	{
            return ".";
	}
#endif

/**
 * checks if a file is ext extension
 */
bool CmdSupport::isExtensionFile(string file,string ext)
{
	string real_ext;
	int len_ext=ext.length();
	int len_file=file.length();
	if(len_ext>=len_file){
		return false;
	}
	real_ext=file.substr(len_file-len_ext,len_file);
	return (real_ext==ext);
}

/*
 * gets the filename for a entire path
 */
string CmdSupport::getFileName(string path)
{
	int len=path.length();
	return path.substr(path.find_last_of('/')+1,len);
}
/**
 * gets the system include path
 */
string CmdSupport::getSystemIncludesPath()
{
	string includes;
	string path=".";
#ifdef __linux__
	path=CmdSupport::getexepath();
	path=path.substr (0,path.find_last_of('/',path.length())+1);
#else
	path="../vlabugger/llvm-3.4/";
	//printf("probably you need to specify default include directories with -I option\n");
#endif
	includes=path;
#ifndef NDEBUG
        
        path=path.substr(0,path.find("build",0));
	includes=path+"build/lib/clang/3.4/include/";
#else
        //TODO FIX THIS WHEN THE PACKAGE IS READY
	//includes=includes+"../lib/include";
        path=path.substr(0,path.find("build",0));
	includes=path+"build/lib/clang/3.4/include/";
#endif
	return includes;
}


/**
 * gets the module from a bitcode file
 */
llvm::Module * getModuleFromBitCode(string file)
{
	// S bitcode
	llvm::LLVMContext &Context = llvm::getGlobalContext();
	llvm::SMDiagnostic Err;
	// std::auto_ptr<llvm::Module> *M=new std::auto_ptr<llvm::Module>();
	// M->reset(ParseIRFile(file, Err, Context));
	llvm::Module *M = llvm::ParseIRFile(file,Err,Context);
	if (M == 0) {
		Err.print("vlabugger", errs());
		exit(VB_ERR_BITCODE_COMPILE_ERROR);
	}
	return M;
}

#ifdef VB_SYSTEM_CLANG
/**
 * gets the module from a c/cpp file calling clang by system
 */
llvm::Module * getModuleFromCOrCPPCode(string file,string AddIncludes){

    std::string command="";
    string includes=CmdSupport::getSystemIncludesPath();
    string assemblerfile="/tmp/"+CmdSupport::getFileName(file)+".s.";
    char *temp_file_name=(char*)malloc(sizeof(char)*(assemblerfile.length()+7));
    strncpy(temp_file_name,assemblerfile.c_str(),assemblerfile.length()+6);
    strcat(temp_file_name,"XXXXXX");
    close(mkstemp(temp_file_name));
    assemblerfile=temp_file_name;
    free(temp_file_name);

    command = command.append("$CLANG "
//                             "-g " 
                             "-c " 
                             "-std=c99 " 
                             "-emit-llvm ");
    command = command.append(file);

   // command = command.append(" -target ");
//  command = command.append("x86_64-unknown-linux-gnu");// DONT FORGET TO ADD INCLUDE FOLDER FOR 32 BIT PCS
  //  command = command.append(llvm::sys::getDefaultTargetTriple().c_str());
    // TODO INCLUDE ONLY 64 BIT INCLUDES using -isysroot
    command= command.append(" -isysroot ");
    command=command.append(includes);

    command= command.append(" -iwithprefixbefore ");
    command=command.append(includes);

    command= command.append(" -iwithprefix ");
    command=command.append(includes);

    command= command.append(" -iwithsysroot ");
    command=command.append(includes);        

    if(AddIncludes!="-"){
            command= command.append(" -I ");
            command= command.append(AddIncludes);
    }
    command = command.append(" -S ");
    command = command.append(" -o ");
    command = command.append(assemblerfile);
    command = command.append(" -Werror");
    if(system(command.c_str())!=0){
            printf("error executing clang\n");
            exit(VB_ERR_CLANG_COMPILE_ERROR);
    }
    //get module from assambly
    return getModuleFromBitCode(assemblerfile);
}

/**
 * makes clang frees after vlabugger execution
 */
void CmdSupport::freeClangMemory(llvm::Module * M)
{
    M->~Module();
}

#else

static llvm::OwningPtr<clang::CodeGenAction> *Act=0;


/**
 * makes clang frees after vlabugger execution
 */
void CmdSupport::freeClangMemory(llvm::Module * M)
{
    M->~Module();
//    if(Act){
//        Act->~OwningPtr();
//    }
}
/**
 * gets the module from c/cpp file calling clang by library
 */
llvm::Module * getModuleFromCOrCPPCode(string file,string AddIncludes){
        
        clang::CompilerInvocation * CI=0;
        clang::DiagnosticIDs * Cdiagnostics=0;
        
        // C code
	// Arguments to pass to the clang frontend
        vector<const char *> args;
	args.push_back(file.c_str());
//	args.push_back("-g");
	args.push_back("-c");
	args.push_back("-std=c99");
//	args.push_back("-Werror");
//	args.push_back("-target");
//	args.push_back(llvm::sys::getDefaultTargetTriple().c_str());
	string includes=CmdSupport::getSystemIncludesPath();
	args.push_back("-isysroot");
	args.push_back(includes.c_str());
        
	args.push_back("-iwithprefixbefore");
	args.push_back(includes.c_str());

	args.push_back("-iwithprefix");
	args.push_back(includes.c_str());

	args.push_back("-iwithsysroot");
	args.push_back(includes.c_str());
        

	if(AddIncludes!="-"){
		args.push_back("-I");

#ifdef __linux__
		args.push_back(AddIncludes.c_str());
#else
		args.push_back("/usr/include/");
#endif

	}

	// The compiler invocation needs a DiagnosticsEngine so it can report problems
        
        Cdiagnostics=new clang::DiagnosticIDs();
	llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID(Cdiagnostics);

	CI=clang::createInvocationFromCommandLine	(args);
       

	//Now you need a CompilerInstance. (Yes, the Clang API has a class called Compilation, a class called CompilerInvocation, and a class called CompilerInstance.)
	//The frontend classes, CompilerInvocation and CompilerInstance,
	//play a similar role as the driver classes, Driver and Compilation, used in the above example. Both the frontend classes and the driver classes take some command-line-style arguments and then compile some code.
	//One important difference between them is that the driver classes can invoke other tools like ld,
	//whereas the frontend classes can only handle tasks native to Clang. Returning now to the example code,
	//the next step is to construct the CompilerInstance and associate it with the CompilerInvocation:

	clang::CompilerInstance Clang;
	Clang.setInvocation(CI);
	//	   DiagnosticOptions DiagOpts;
	//	   TextDiagnosticPrinter *diagprinter=new  TextDiagnosticPrinter(llvm::dbgs(), &DiagOpts);
	//	   DiagnosticsEngine * den=new DiagnosticsEngine (DiagID, &DiagOpts,diagprinter);
	//	   IntrusiveRefCntPtr<PreprocessorOptions> PPOpts = new PreprocessorOptions();
	//	   LangOptions *lops=new LangOptions();
	//	   TargetOptions tops;
	//	   tops.Triple="x86_64-unknown-linux-gnu";
	//	   tops.CPU="x86-64";
	//	   tops.ABI="GNU";
	//	   TargetInfo * TargInfo = TargetInfo::CreateTargetInfo(*den,tops);
	//	   FileSystemOptions FileSystemOpts;
	//	   FileSystemOpts.WorkingDir=".";
	//	   FileManager *fm=new  FileManager(FileSystemOpts);
	//	   SourceManager *sm=new SourceManager (*den,*fm);
	//	   IntrusiveRefCntPtr< HeaderSearchOptions > HSOpts;
	//
	//	   HeaderSearch *hsearch=new HeaderSearch (HSOpts, *fm, *den, *lops, TargInfo);
	//
	//	   clang::Preprocessor *ppcsr=new clang::Preprocessor(PPOpts, *den, *lops, TargInfo, *sm, *hsearch, Clang);
	//	   Clang.setPreprocessor(ppcsr);
	//Set up diagnostics so the CompilerInstance can report problems:

        Clang.createDiagnostics();
	if (!Clang.hasDiagnostics()){
		exit(VB_ERR_CLANG_COMPILE_ERROR);
	}

	//Create an action for the compiler to carry out. A frontend ���action��� is a little like a driver ���task���,
	//in that it���s a step to be carried out while building a program. A task is something like ���compile���, ���assemble���, ���link���,
	//whereas an action is something like ���dump AST���, ���emit assembly���, ���emit bitcode���, ���print preprocessed input���.
	//In the Clang source code, you can see a list of all actions in lib/FrontendTool/ExecuteCompilerInvocation.cpp.
	//For this example, the action is ���emit LLVM only���:

	Act=new llvm::OwningPtr<clang::CodeGenAction>(new clang::EmitLLVMOnlyAction());
	//Carry out the action:

	if (!Clang.ExecuteAction(**Act)){
		exit(VB_ERR_CLANG_COMPILE_ERROR);
	}
	//Grab the resulting Module:
	llvm::Module *module = (*Act)->takeModule();
        return module;
}

#endif

/**
 * get the llvm module from the input file
 */
llvm::Module * CmdSupport::getModuleFromFile(string InputFilename,string AddIncludes)
{
	// Load the input module...
	if((isExtensionFile(InputFilename,".c")
			||(isExtensionFile(InputFilename,".cpp")))){
		return getModuleFromCOrCPPCode(InputFilename,AddIncludes);
	}
#ifndef NDEBUG
	if((isExtensionFile(InputFilename,".s"))
			||(isExtensionFile(InputFilename,".ll"))){
		return getModuleFromBitCode(InputFilename);
	}
#endif
	printf("vlabugger cant recognice this kind of file %s\n",InputFilename.c_str());
	exit(VB_ERR_CMD_ARGUMENTS_ERROR);
	return 0;
}

/**
 * 
 * @param c_code input c code
 * @return the module IR
 */
llvm::Module * CmdSupport::getModuleFromCCode(string c_code)
{
    string c_code_file="/tmp/vlabugger_internal_c_code.";
    char *temp_file_name=(char*)malloc(sizeof(char)*(c_code_file.length()+7));
    string ErrorInfo;
	llvm::raw_fd_ostream * c_code_arch = NULL;
    
    
    strncpy(temp_file_name,c_code_file.c_str(),c_code_file.length()+6);
    strcat(temp_file_name,"XXXXXX");
    close(mkstemp(temp_file_name));
    c_code_file=temp_file_name;
    free(temp_file_name);
    c_code_file+=".c";
    
    c_code_arch = new llvm::raw_fd_ostream( c_code_file.c_str(),ErrorInfo, sys::fs::F_Binary);
    (*c_code_arch)<<c_code<<"\n";
    c_code_arch->flush();
    c_code_arch->close();
    
    return CmdSupport::getModuleFromFile(c_code_file,"");
}

/**
 * hides de linked non used options
 */
void CmdSupport::hide_linked_cmd_opts()
{
    StringMap<cl::Option*> Map;
    cl::getRegisteredOptions(Map);
    Map["disable-debug-info-verifier"]->setHiddenFlag(cl::Hidden);
    Map["enable-tbaa"]->setHiddenFlag(cl::Hidden);
    Map["print-after-all"]->setHiddenFlag(cl::Hidden);
    Map["print-before-all"]->setHiddenFlag(cl::Hidden);
    Map["time-passes"]->setHiddenFlag(cl::Hidden);
    Map["verify-dom-info"]->setHiddenFlag(cl::Hidden);
    Map["verify-loop-info"]->setHiddenFlag(cl::Hidden);
    Map["verify-region-info"]->setHiddenFlag(cl::Hidden);  
    Map["verify-scev"]->setHiddenFlag(cl::Hidden);
    Map["enable-correct-eh-support"]->setHiddenFlag(cl::Hidden);
    
    //only with library clang linked
#ifndef VB_SYSTEM_CLANG
    Map["fatal-assembler-warnings"]->setHiddenFlag(cl::Hidden);
    Map["fdata-sections"]->setHiddenFlag(cl::Hidden);
    Map["internalize-public-api-file"]->setHiddenFlag(cl::Hidden);
    Map["internalize-public-api-list"]->setHiddenFlag(cl::Hidden);
    Map["ffunction-sections"]->setHiddenFlag(cl::Hidden);
    Map["stats"]->setHiddenFlag(cl::Hidden);
    Map["enable-load-pre"]->setHiddenFlag(cl::Hidden);
    Map["enable-objc-arc-opts"]->setHiddenFlag(cl::Hidden);
    Map["bounds-checking-single-trap"]->setHiddenFlag(cl::Hidden);
#endif
      
}