#include <iostream>
#include <string>

#include "llvm/Support/CommandLine.h"
#include "llvm/PassManager.h"
#include "llvm/Transforms/Scalar.h"

#include "vlabugger/vlabuggerExitErrorCodes.h"
#include "vlabugger/Support/CmdSupport/CmdSupport.h"
#include "vlabugger/ObfuscatingPass/ObfuscatingPass.h"
#include "vlabugger/Backend/Backend.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Signals.h"

using namespace std;
using namespace llvm;
using namespace vlabugger;


static cl::list<std::string>
inputs(cl::Positional, cl::desc("<inputs c|cpp files>"),cl::Required);

static cl::opt<std::string>
output("o", cl::desc("Override output filename"),
               cl::value_desc("filename"),cl::init("output.o"));


static cl::opt<bool>
obfuscate("obfuscate",cl::desc("obfuscate the binary code"),cl::init(false));

static cl::opt<std::string>
include_folder("I", cl::desc("Include directory"),
               cl::value_desc("Include dir"),cl::init(""));


/**
 * prints the actual version of vlabugger
 */
void version_print()
{
    cout<<"vlabugger "<<VLABUGGER_VERSION<<std::endl;
    cout<<"By VLA Team"<<std::endl;
}

/**
 * executes de debugging passes for vlabugger over the module M
 * @param M the module
 */
void executeVlabuggerPasses(Module *M)
{

	PassRegistry &Registry = *llvm::PassRegistry::getPassRegistry();

	PassManager Passes;
	ModulePass * obfuscator;

	// Initialize llvm core
	llvm::initializeCore(Registry);
	llvm::initializeAnalysis(Registry);
	llvm::initializeIPA(Registry);
	llvm::initializeTransformUtils(Registry);
        
	// Create a PassManager to hold and optimize the collection of passes we are
	// about to build
        if(obfuscate){
            obfuscator=vlabugger::createObfuscatingPass();
            Passes.add(obfuscator);
        }

	// Now that we have all of the passes ready, run them.
	Passes.run(*M);
}


/**
 * main for vlabugger debugger
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc,const char **argv){
    cl::SetVersionPrinter(version_print);
    vlabugger::CmdSupport::hide_linked_cmd_opts();
    cl::ParseCommandLineOptions(argc, argv,"vlabugger A debugger based on LLVM\n");   
    sys::PrintStackTraceOnErrorSignal();
    llvm::PrettyStackTraceProgram x(argc,argv);
    //vlabugger::CmdSupport::capture_vb_signals();
    for(vector<std::string>::iterator I=inputs.begin(),E=inputs.end();I!=E;++I){
        string input=*I;
        Module* M=vlabugger::CmdSupport::getModuleFromFile(input,include_folder);
        executeVlabuggerPasses(M);
        Backend* backend = new Backend(M,output);
        backend->doCompilation();
     }
    return 0;
}
