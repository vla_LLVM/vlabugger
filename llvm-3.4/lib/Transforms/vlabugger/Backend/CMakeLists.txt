include_directories("${PATH_TO_LLVM_BUILD}/include" 
 						"${LLVM_MAIN_INCLUDE_DIR}" 
 						"${LLVM_MAIN_SRC_DIR}/tools/clang/include" 
 						"${CMAKE_BINARY_DIR}/tools/clang/include")


add_llvm_library(LLVMBackend
  Backend.cpp
  )

target_link_libraries(LLVMBackend clangFrontendTool)
  
